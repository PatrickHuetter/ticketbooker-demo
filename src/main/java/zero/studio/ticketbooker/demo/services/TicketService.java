package zero.studio.ticketbooker.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zero.studio.ticketbooker.demo.modells.Ticket;
import zero.studio.ticketbooker.demo.repositories.TicketRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TicketService {
    private final TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    public Optional<Ticket> getTicketByID(long ID) {
        return ticketRepository.findById(ID);
    }

    public Ticket bookTicket(Long id) {
        Optional<Ticket> optionalTicket = ticketRepository.findById(id);
        if (optionalTicket.isPresent()) {
            Ticket ticket = optionalTicket.get();
            if (!ticket.isBooked()) {
                ticket.setBooked(true);
                return ticketRepository.save(ticket);
            }
        }
        return null;
    }

    public Ticket save(Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    public List<Ticket> getUnbookedTickets() {
        List<Ticket> unbookedTickets = new ArrayList<>();
        ticketRepository.findAll().forEach( ticket -> {
            if (!ticket.isBooked()){
                unbookedTickets.add(ticket);
            }
        });
        return unbookedTickets;
    }
}
