package zero.studio.ticketbooker.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zero.studio.ticketbooker.demo.modells.User;
import zero.studio.ticketbooker.demo.repositories.UserRepository;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByID(long id) {
        Optional user = userRepository.findUser(id);

        if (!user.isEmpty()) {
            return (User) user.get();
        }
        return null;
    }

}
