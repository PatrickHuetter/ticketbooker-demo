package zero.studio.ticketbooker.demo.modells;

import jakarta.persistence.*;

@Entity
@Table(name = "tickets")
public class Ticket {
    @Id
    private Long id;
    private String src;
    private String dest;
    private boolean isBooked = false;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Ticket() {
    }

    public Ticket(Long id, String src, String dest, String userId) {
        this.id = id;
        this.src = src;
        this.dest = dest;
        this.isBooked = isBooked;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", src='" + src + '\'' +
                ", dest='" + dest + '\'' +
                ", isBooked=" + isBooked +
                '}';
    }

}
