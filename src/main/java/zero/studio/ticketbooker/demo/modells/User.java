package zero.studio.ticketbooker.demo.modells;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "users")
@Component
public class User {
    private String firstName;
    private String lastName;
    private int age;

    @Id
    @GeneratedValue
    private Long id;

    public User() {
    }

    public User(String firstName, String lastName, int age, Long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstNme='" + firstName + '\'' +
                ", lastNme='" + lastName + '\'' +
                ", age=" + age +
                ", id=" + id +
                '}';
    }
}
