package zero.studio.ticketbooker.demo.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import zero.studio.ticketbooker.demo.modells.Ticket;
import zero.studio.ticketbooker.demo.services.TicketService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;


import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/tickets")
public class TicketController {
    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }


    @GetMapping({"/", ""})
    public String getAllTicketsTest(Model model) {
        List<Ticket> tickets = ticketService.getAllTickets();
        model.addAttribute("tickets", tickets);

        return "Tickets";
    }

    @GetMapping("/{id}")
    public ResponseEntity<Ticket> getTicketById(@PathVariable Long id) {
        Optional<Ticket> ticket = ticketService.getTicketByID(id);
        return ticket.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @PostMapping("/book")
    public ModelAndView processForm(@RequestBody Map<String, Long> request) {

        System.out.println(request.get("id"));


        ModelAndView modelAndView = new ModelAndView("Tickets");

        // it is made such way, only cause i can`t reach need-able result another way wight now;
        boolean isBooked = ticketService.bookTicket(request.get("id")).isBooked();

        if (isBooked) {
            modelAndView.addObject("message", "Билет с ID " + request.get("id") + " успешно забронирован");
            return modelAndView;
        } else {
            modelAndView.addObject("message", "Билет с ID " + request.get("id") + " не был забронирован, так как он уже забронирован");
            return modelAndView;
        }
    }

    @GetMapping("/unbooked")
    public ResponseEntity<List<Ticket>> getUnbookedTickets() {
        List<Ticket> unbookedTickets = ticketService.getUnbookedTickets();
        return ResponseEntity.ok(unbookedTickets);
    }


}
