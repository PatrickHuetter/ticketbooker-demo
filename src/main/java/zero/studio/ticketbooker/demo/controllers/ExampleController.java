package zero.studio.ticketbooker.demo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/example")
public class ExampleController {

    @GetMapping("/form")
    public String showForm() {
        return "ExampleForm";
    }

    @PostMapping("/process")
    public ResponseEntity<String> processForm(@RequestBody String data) {
        // Ваша логика обработки данных
        return ResponseEntity.ok("Данные успешно обработаны: " + data);
    }
}
