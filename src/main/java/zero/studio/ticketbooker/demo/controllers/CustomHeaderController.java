package zero.studio.ticketbooker.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zero.studio.ticketbooker.demo.services.UserService;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class CustomHeaderController {

    private UserService userService;

    @Autowired
    public CustomHeaderController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test1")
    public ResponseEntity<?> backId(@RequestParam Long id) {
        return ResponseEntity.ok(id);
    }

    @PostMapping("/test2")
    public ResponseEntity<?> test2(@RequestBody Map<String, Integer> id) {

        System.out.println(userService.getUserByID(id.get("id")));


        return ResponseEntity.ok("");
    }

    @PostMapping("/test3")
    public ResponseEntity<?> handleRequestWithHeader(@RequestHeader("boo") String booHeader, @RequestBody Map<String, Integer> id) {
        return ResponseEntity.ok("Значение заголовка 'boo': " + booHeader + ", " + id.get("id"));
    }

    @PostMapping("/test4")
    public ResponseEntity<?> handleRequestWithResponseHeader(
            @RequestHeader("boo") String booHeader,
            @RequestBody Map<String, Integer> id) {

        ResponseEntity<?> responseEntity = ResponseEntity
                .status(HttpStatus.OK)
                .header("boo", booHeader)
                .body("Response body");

        return responseEntity;
    }

}
