package zero.studio.ticketbooker.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"/", ""})
public class HomeController {

    @GetMapping("")
    public String homeController() {
        return "redirect:/home"; // Перенаправление на "/home"
    }

    @GetMapping("/home")
    public String homeRedirectingController() {
        return "Home";
    }
}
