package zero.studio.ticketbooker.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import zero.studio.ticketbooker.demo.modells.Ticket;

import java.util.List;
import java.util.Optional;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
    // Получить все билеты
    List<Ticket> findAll();

    // Получить билет по ID
    Optional<Ticket> findById(Long id);

    // Сохранить билет
    Ticket save(Ticket ticket);
}
