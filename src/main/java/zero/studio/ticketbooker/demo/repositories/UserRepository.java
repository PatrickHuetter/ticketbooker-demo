package zero.studio.ticketbooker.demo.repositories;

import org.springframework.stereotype.Repository;
import zero.studio.ticketbooker.demo.modells.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    private List<User> userList = new ArrayList<>();

    {
        for (long i = 0; i <= 10; i++) {
            userList.add(new User("firstName" + i, "lastName" + i, (int) (18 + i), i));
        }
    }

    public Optional<User> findUser(Long id) {
        Optional optional = Optional.empty();
        for (User user : userList) {
            if (user.getId() == id) {
                optional = Optional.of(user);
                return optional;
            }
        }
        return optional;
    }

}
