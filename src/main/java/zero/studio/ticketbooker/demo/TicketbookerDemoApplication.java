package zero.studio.ticketbooker.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketbookerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketbookerDemoApplication.class, args);
    }

}
